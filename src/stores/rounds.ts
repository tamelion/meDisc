import { useCoursesStore } from './courses'
import type { Round, Score } from '@/types'
import { defineStore } from 'pinia'

interface State {
  rounds: Array<Round>
}

export const useRoundsStore = defineStore('rounds', {
  state: () => {
    const rounds: Array<Round> = []
    const state: State = { rounds }
    return state
  },
  getters: {
    maxId: (state) => {
      if (state.rounds.length > 0) {
        return Math.max(...state.rounds.map((el) => el.id))
      }
      return 0
    },
    getRoundById: (state) => {
      return (roundId: number) => state.rounds.find((round) => round.id === roundId)
    }
  },
  actions: {
    createRound(userId: number, courseId: number, date: Date) {
      const courses = useCoursesStore()
      const course = courses.getCourseById(courseId)
      const score: Array<Score> = []

      if (course) {
        for (const hole of course.holes) {
          score.push({
            hole: {
              courseId: course.id,
              number: hole.number,
              displayName: hole.displayName,
              distance: hole.distance,
              par: hole.par
            },
            strokes: null
          })
        }
        const round = {
          id: this.maxId + 1,
          userId: userId,
          date,
          course: {
            id: course.id,
            name: course.name
          },
          score
        }
        this.rounds.push(round)
        return round
      } else {
        console.error(`Course with id ${courseId} does not exist`)
      }
    },
    updateScore(roundId: number, holeNumber: number, strokes: number) {
      const round = this.rounds.find((round) => round.id === roundId)
      const score = round?.score?.find((score: Score) => score.hole.number === holeNumber)

      if (score) {
        score.strokes = strokes
      }
    },
    deleteRound(round: Round) {
      const index = this.rounds.findIndex((el) => el.id === round.id)
      this.rounds.splice(index, 1)
    },
    importRounds(rounds: Array<Round>) {
      this.rounds.splice(0, this.rounds.length)

      for (const round of rounds) {
        this.rounds.push(round)
      }
    }
  },
  persist: true
})
