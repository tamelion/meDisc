import RoundEdit from '@/views/RoundEdit.vue'
import RoundNew from '@/views/RoundNew.vue'
import RoundView from '@/views/RoundView.vue'
import RoundsList from '@/views/RoundsList.vue'
import UserNew from '@/views/UserNew.vue'
import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'default',
      component: RoundsList
    },
    {
      path: '/rounds',
      name: 'RoundsList',
      component: RoundsList
    },
    {
      path: '/rounds/new',
      name: 'RoundNew',
      component: RoundNew
    },
    {
      path: '/rounds/:id',
      name: 'RoundView',
      component: RoundView
    },
    {
      path: '/rounds/:roundId/hole/:holeId',
      name: 'RoundEdit',
      component: RoundEdit
    },
    {
      path: '/users/new',
      name: 'UserNew',
      component: UserNew
    }
  ]
})

export default router
