import type { User } from '@/types'
import { defineStore } from 'pinia'

interface State {
  user: User | null
}

export const useUserStore = defineStore('user', {
  state: () => {
    const state: State = { user: null }
    return state
  },
  getters: {
    getUserById: (state) => {
      // TODO: We need to implement proper users
      return (userId: number) => state.user //eslint-disable-line @typescript-eslint/no-unused-vars
      // return (userId: number) => state.rounds.find((user) => user.id === userId)
    }
  },
  actions: {
    add(name: string, email: string) {
      this.user = { id: 1, name, email }
    }
  },
  persist: true
})
