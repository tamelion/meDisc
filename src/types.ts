export interface Course {
  id: number
  name: string
  nrOfHoles: number
  holes: Array<Hole>
  location: Location
}

export interface Hole {
  courseId: number
  number: number
  displayName: string
  distance: number
  par: number
}

export interface Location {
  city: string
  state: string
}
export interface Round {
  id: number
  userId: number
  date: Date
  course: {
    id: number
    name: string
  }
  score: Array<Score>
}

export interface Score {
  hole: Hole
  strokes: number | null
}

export interface User {
  id: number
  name: string
  email: string
}
